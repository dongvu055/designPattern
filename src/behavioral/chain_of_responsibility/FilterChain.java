package behavioral.chain_of_responsibility;

import java.util.Objects;

public abstract class FilterChain {
    protected FilterChain filterChain;

    public void doFilter() {
        doFilterInternal();
        if (!Objects.isNull(filterChain))
            filterChain.doFilter();
    }

    public void nextFilter(FilterChain filterChain) {
        this.filterChain = filterChain;
    }

    protected abstract void doFilterInternal();
}
