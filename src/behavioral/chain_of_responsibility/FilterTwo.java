package behavioral.chain_of_responsibility;

public class FilterTwo extends FilterChain{
    @Override
    protected void doFilterInternal() {
        System.out.println("Filter Two");
    }
}
