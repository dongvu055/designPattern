package behavioral.chain_of_responsibility;

public class FilterThree extends FilterChain{
    @Override
    protected void doFilterInternal() {
        System.out.println("Filter Three");
    }
}
