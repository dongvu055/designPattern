package behavioral.chain_of_responsibility;

public class FilterOne extends FilterChain{
    @Override
    protected void doFilterInternal() {
        System.out.println("Filter One");
    }
}
