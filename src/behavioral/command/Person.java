package behavioral.command;

public class Person {
    private String personName;

    public Person(String personName) {
        this.personName = personName;
    }

    public void walk() {
        System.out.println(personName + " is Walking");
    }

    public void swim() {
        System.out.println(personName + " is Swimming");
    }
}
