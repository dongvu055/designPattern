package behavioral.command;

import java.util.ArrayList;
import java.util.List;

public class PersonInvoker {
    private List<ActivePerson> abstractPeople = new ArrayList<>();

    public void addActivePerson(ActivePerson activePerson) {
        abstractPeople.add(activePerson);
    }

    public void active() {
        abstractPeople.forEach(activePerson -> activePerson.active());
    }
}
