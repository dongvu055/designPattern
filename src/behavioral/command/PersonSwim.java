package behavioral.command;

public class PersonSwim extends ActivePerson {
    private Person person;

    public PersonSwim(Person person) {
        this.person = person;
    }

    @Override
    public void active() {
        person.swim();
    }
}
