package behavioral.command;

public abstract class ActivePerson {
    public abstract void active();
}
