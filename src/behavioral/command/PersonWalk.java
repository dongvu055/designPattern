package behavioral.command;

public class PersonWalk extends ActivePerson {
    private Person person;

    public PersonWalk(Person person) {
        this.person = person;
    }

    @Override
    public void active() {
        person.walk();
    }
}
