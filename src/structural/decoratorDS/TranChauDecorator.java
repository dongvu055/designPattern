package structural.decoratorDS;

public class TranChauDecorator extends TraDecorator{
    public TranChauDecorator(Tra tra) {
        super(tra);
    }

    @Override
    public void doTra() {
        tra.doTra();
        System.out.print(" Tran chau");
    }
}
