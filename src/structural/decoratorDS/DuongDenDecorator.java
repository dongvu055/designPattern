package structural.decoratorDS;

public class DuongDenDecorator extends TraDecorator{
    public DuongDenDecorator(Tra tra) {
        super(tra);
    }

    @Override
    public void doTra() {
        tra.doTra();
        System.out.print(" Duong den");
    }
}
