package structural.decoratorDS;

public class Duong30Decorator extends TraDecorator {
    public Duong30Decorator(Tra tra) {
        super(tra);
    }

    @Override
    public void doTra() {
        tra.doTra();
        System.out.print(" 30% Duong");
    }
}
