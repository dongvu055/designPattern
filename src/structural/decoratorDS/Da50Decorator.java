package structural.decoratorDS;

public class Da50Decorator extends TraDecorator{
    public Da50Decorator(Tra tra) {
        super(tra);
    }

    @Override
    public void doTra() {
        tra.doTra();
        System.out.print(" 50% Da");
    }
}
