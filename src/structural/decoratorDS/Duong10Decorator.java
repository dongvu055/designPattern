package structural.decoratorDS;

public class Duong10Decorator extends TraDecorator{
    public Duong10Decorator(Tra tra) {
        super(tra);
    }

    @Override
    public void doTra() {
        tra.doTra();
        System.out.print(" 10% Duong");
    }
}
