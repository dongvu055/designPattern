package structural.decoratorDS;

public abstract class TraDecorator implements Tra {
    protected Tra tra;

    public TraDecorator(Tra tra) {
        this.tra = tra;
    }
}
