package structural.decoratorDS;

public class Da30Decorator extends TraDecorator {
    public Da30Decorator(Tra tra) {
        super(tra);
    }

    @Override
    public void doTra() {
        tra.doTra();
        System.out.print(" 30% Da");
    }
}
