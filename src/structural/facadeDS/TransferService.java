package structural.facadeDS;

public class TransferService {
    public static void transfer(int amount, AccountService accountServiceFrom, AccountService accountServiceTo) {
        System.out.println("Transfer:" + amount + " From:" + accountServiceFrom.getAccountName() + " To:" + accountServiceTo.getAccountName());
    }
}
