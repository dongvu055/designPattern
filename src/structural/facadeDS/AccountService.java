package structural.facadeDS;

public class AccountService {
    private String accountName;

    public String getAccountName() {
        return accountName;
    }

    public void setAccountName(String accountName) {
        this.accountName = accountName;
    }

    public AccountService(String accountName) {
        this.accountName = accountName;
    }

    public AccountService getAccount(String accountName) {
        return new AccountService(accountName);
    }
}
