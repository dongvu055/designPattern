package structural.facadeDS;

public interface BankServiceFacade {
    void moneyTransfer(Integer amount,String accountFrom,String accountTo);
}
