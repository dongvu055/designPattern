package structural.facadeDS;

public class BankServiceFacadeImpl implements BankServiceFacade{
    @Override
    public void moneyTransfer(Integer amount,String accountFrom,String accountTo) {
        AccountService accountService = new AccountService(accountFrom);
        AccountService accountService2 = new AccountService(accountTo);
        if (PaymentService.doPayment()){
            TransferService.transfer(amount,accountService,accountService2);
        }
    }
}
