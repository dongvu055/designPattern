package structural.proxyDS;

import java.util.Objects;

public class ProxySavingAccount extends Account{
    private SavingAccount savingAccount;
    @Override
    public void accountType() {
        if (Objects.isNull(savingAccount)){
            savingAccount = new SavingAccount();
        }
        savingAccount.accountType();
    }
}
