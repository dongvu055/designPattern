package structural.proxyDS;

public class SavingAccount extends Account{
    @Override
    protected void accountType() {
        System.out.println("Saving account");
    }
}
